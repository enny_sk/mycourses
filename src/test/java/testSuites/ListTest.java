package testSuites;

import myArrayList.MyList;
import org.junit.Assert;
import org.junit.Test;
import properties.User;

import java.util.List;

/**
 * Created by enny on 01.08.17.
 */
public class ListTest{

    @Test
    public void sizeTestCase() {
        List list = new MyList();
        Assert.assertTrue(list.size() == 0);
    }

    @Test
    public void addTestCase() {
        List list = new MyList();
        list.add("Australia");
        list.add("India");
        list.add("Ukraine");
        Assert.assertTrue("not added element", list.size() == 3);
    }

    @Test
    public void clearTestCase() {
        List list = new MyList();
        list.add("Australia");
        list.add("India");
        list.clear();
        Assert.assertTrue("list not cleared", list.size() == 0);
    }

    @Test
    public void isEmptyTestCase() {
        List list = new MyList();
        Assert.assertTrue("list is not empty", list.size() == 0);
    }

    @Test
    public void removeTestCase() {
        List list = new MyList();
        list.add("Water");
        list.add("Juice");
        list.add("Coffee");
        list.add("Tea");
        list.remove("Tea");
        Assert.assertTrue("Incorrect list size after remove", list.size() == 3);
        for (Object o : list.toArray()) {
            System.out.println(o);
        }
    }

    @Test
    public void removeNoExistTestCase() {
        List list = new MyList();
        list.add("Water");
        list.add("Juice");
        list.add("Coffee");
        list.add("Tea");
        Assert.assertTrue("There is no such element", list.remove("Tea"));
        Assert.assertTrue("Incorrect list size", list.size() == 3);
    }

    @Test
    public void containsCheckTestCase() {
        List list = new MyList();
        list.add("Hello");
        list.add(2);
        list.add("Chocolate");
        list.add("10");
        Assert.assertTrue("There is no such element", list.contains("Chocolate"));
    }

    @Test
    public void getTestCase() {
        List list = new MyList();
        list.add("Hello");
        list.add("2");
        list.add("Chocolate");
        list.add("10");
        Assert.assertEquals("Incorrect element in list", "Chocolate", list.get(2));
    }

    @Test
    public void setTestCase() {
        List list = new MyList();
        list.add("Hello");
        list.add("2");
        list.add("Chocolate");
        list.add("10");
        list.set(3, "SET");
        Assert.assertEquals("Element do not set in list", "SET", list.get(3));
    }
}
