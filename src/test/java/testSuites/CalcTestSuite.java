package testSuites;

import myArrayList.Calc;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by enny on 15.07.17.
 */
public class CalcTestSuite {
    @Test
    public void testSum() throws Exception{
        Calc calc = new Calc();

        double result = calc.getResult(5,"+", 4);
        Assert.assertEquals(result, 9.0);
    }
    @Test
    public void testSubtraction() throws Exception{
        Calc calc = new Calc();

        double result = calc.getResult(5,"-", 4);
        Assert.assertEquals(result, 1.0);
    }
    @Test
    public void testMultiply() throws Exception{
        Calc calc = new Calc();

        double result = calc.getResult(5,"*", 4);
        Assert.assertEquals(result, 20.0);
    }
    @Test
    public void testDivide() throws Exception{
        Calc calc = new Calc();

        double result = calc.getResult(5,"/", 4);
        Assert.assertEquals(result, 1.25);
    }
}
