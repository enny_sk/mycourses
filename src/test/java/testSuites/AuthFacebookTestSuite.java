package testSuites;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AuthPage;
import pages.FacebookAuthPage;
import pages.IndexPage;
import properties.User;
import services.CustomService;

/**
 * Created by enny on 28.08.17.
 */
public class AuthFacebookTestSuite extends BaseTest{
    @Test void testLoginWithFacebook(){
        User user = new User();
        CustomService service = new CustomService(driver);
        service.goTo("https://www.templatemonster.com/");

        IndexPage indexPage = PageFactory.initElements(driver, IndexPage.class);
        AuthPage authPage = PageFactory.initElements(driver, AuthPage.class);
        FacebookAuthPage facebookAuthPage = PageFactory.initElements(driver, FacebookAuthPage.class);
        indexPage.waitForHeartEnable();
        indexPage.clickOnSignLink();
        service.switchToLastWindow();
        service.waitUrl("account");
        authPage.clickOnFacebookLoginButton();
        service.switchToLastWindow();
        service.waitUrl("facebook");
        facebookAuthPage.login(user);
        service.switchToLastWindow();
        CustomService.waitForCookie("access_token", driver);
        CustomService.waitForCookie("cid", driver);
        Assert.assertEquals(driver.manage().getCookieNamed("cid").getValue(),user.getCid(), "Login does not work, not found cookies cid");
    }

}

