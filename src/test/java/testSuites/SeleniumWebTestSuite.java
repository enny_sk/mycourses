package testSuites;

import pages.IndexPage;
import properties.User;
import services.CustomService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by enny on 28.08.17.
 */
public class SeleniumWebTestSuite extends BaseTest {
    @Test
     public void testXpath() {

       driver.get("https://www.templatemonster.com");

        WebElement favoriteHart = driver.findElement(By.id("menu-favorites"));
        List<WebElement> subMenu = driver.findElements(By.xpath(".//*[contains(@class, 'sub-menu container')]//*[contains(@class, 'sub-menu-1_dropdown')]/ul/li"));

        WebElement categoriesFashions = driver.findElement(By.id("categories-fashions"));
        WebElement heartOnThirdTemplate = driver.findElement(By.xpath(".//*[@id='tab-bestseller']//*[@class='container']//*[contains(@class, 'js-wizard-search-results')]/ul/li[3]//*[contains(@class, 'tm-icon')]"));
 }

    @Test
    public void goToAuthPage(){
        CustomService service = new CustomService(driver);

        service.goTo("https://www.templatemonster.com/");

        IndexPage indexPage = PageFactory.initElements(driver, IndexPage.class);

        indexPage.waitForHeartEnable();

        indexPage.clickOnSignLink();

        service.switchToLastWindow();
        service.waitUrl("account");

        Assert.assertEquals(driver.getCurrentUrl(), "https://account.templatemonster.com/auth/#/", "Incorrect current url");
    }
}