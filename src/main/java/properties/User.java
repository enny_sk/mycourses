package properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by enny on 01.09.17.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor

public class User {
    private String name;
    private int age;
    private String email = "enny@templatemonster.me";
    private String passwordTM ="123357";
    private String passwordFacebook ="wd'njxtr";
    private String cid ="iOTjRfEV8yx4N6K3pa6rn6otq7qctO64L1U95YZnN6M89qN9I7";
}
