package myArrayList;

import java.util.Scanner;

/**
 * Created by enny on 05.07.17.
 */
public class Calc {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter first number:");
        double numberOne = sc.nextDouble();
        System.out.println("Please enter operation");
        String op = sc.next();
        System.out.println("Please enter second number");
        double numberTwo = sc.nextDouble();
        sc.close();
        try {
            double res = getResult(numberOne, op, numberTwo);
            System.out.println(res);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static  double getResult(double numberOne, String op, double numberTwo) throws Exception {
        switch (op) {
            case "+":
                return numberOne + numberTwo;
            case "-":
                return numberOne - numberTwo;
            case "*":
                return numberOne * numberTwo;
            case "/":
                return numberOne / numberTwo;
            default:
                throw new Exception("Please choose one of this: + , - , * , /");

        }
    }
}
