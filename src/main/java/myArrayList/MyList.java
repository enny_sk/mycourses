package myArrayList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by enny on 01.08.17.
 */
public class MyList implements List<Object> {
    Object[] array;
    int size;
    int capacity=10;


    public MyList(){
        array = new Object[capacity];
    }

    public int size() {
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }


    public boolean contains(Object o) {
        if (o==null)  return false;
        for (int i=0; i<size; i++){
            if (array[i].equals(o))
                return true;
        }
        return false;
    }

    public Iterator iterator() {
        return new MyIterator();
    }

       public Object[] toArray() {
        return array;
    }


    public Object[] toArray(Object[] a){
        return new Object[0];
    }

    public boolean add(Object obj){
        if (obj == null) {
            return false;
        }
        if (size <capacity) {
            array[size] = obj;
            size ++;
        }
        else {
            capacity+=10;
            Object[] biggerArr = new Object[capacity];
            for (int i = 0; i < size; i++) {
                biggerArr[i] = array[i];
            }
            biggerArr[size + 1] = obj;
            array = biggerArr;
        }
        return true;
    }


    public boolean remove(Object o) {
        if (o==null)
            return false;
        for (int i = 0; i < size; i++) {
            if (array[i].equals(o)){
                for (int j = i; j < size; j++){
                    array[j]=array[j+1];
                }
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<?> c) {
        return false;
    }

    public void clear() {
        size=0;
        capacity = 10;
        array=new Object[capacity];
    }


    public boolean addAll(Collection c) {
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            add(iterator.next());
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }


    public Object get(int index) {
        if (index <size){
            return array[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
    public Object set(int index, Object element) {
        if (index < 0 || index >= size)
            throw new ArrayIndexOutOfBoundsException();
        Object oldValue = array[index];
        array[index] = element;
        return oldValue;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (array[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(array[i]))
                    return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    private class MyIterator implements Iterator{
        int index;
        public boolean hasNext() {
            return index!=size;
        }
        public Object next() {
            return array[index++];
        }
    }
}
