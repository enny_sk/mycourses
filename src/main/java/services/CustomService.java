package services;

import org.junit.Assert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



/**
 * Created by enny on 28.08.17.
 */
public class CustomService {
    private WebDriver driver;

    public CustomService(WebDriver driver) {
        this.driver = driver;
    }

    public void waitClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUrl(String url) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void waitUrl2(String url) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until((WebDriver webDriver) -> driver.getCurrentUrl().contains(url));
    }

    public void switchToLastWindow() {
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

    public void goTo(String url) {
        driver.get(url);
    }


    public static void waitForCookie(final String cookieName, final WebDriver driver){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(new ExpectedCondition<Boolean>(){
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.manage().getCookieNamed(cookieName) != null);
                }
            });
        }catch (TimeoutException e){
            Assert.assertTrue("was not found after timeout", true);
        }
    }
}

