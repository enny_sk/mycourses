package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import properties.User;
import services.CustomService;

/**
 * Created by enny on 28.08.17.
 */
public class FacebookAuthPage {
    private final WebDriver driver;
    private CustomService service;

    public FacebookAuthPage(WebDriver driver) {
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy(xpath = ".//input[@id='email']")
    private WebElement emailField;

    @FindBy(xpath =".//input[@id='pass']")
    private WebElement passwordField;

    @FindBy(xpath =".//*[@id='loginbutton']//input[@type='submit']")
    private WebElement loginButton;

    public void enterEmail(String email){
        emailField.sendKeys(email);
    }

    public void enterPassword(String passwordFacebook){
        passwordField.sendKeys(passwordFacebook);
    }
    public void clickLogin() {
        loginButton.click();
    }
    public void login(User user){
        enterEmail(user.getEmail());
        enterPassword(user.getPasswordFacebook());
        clickLogin();
    }
}
