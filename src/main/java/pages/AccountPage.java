package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;

/**
 * Created by enny on 28.08.17.
 */
public class AccountPage {
       private final WebDriver driver;
       private CustomService service;


       public AccountPage(WebDriver driver, CustomService service) {
              this.driver = driver;
              service = new CustomService(driver);
       }



       @FindBy(xpath =".//*[@id='app-account-menu']")
       WebElement accountMenu;

       @FindBy(xpath = ".//*[@id='app-account-menu']//*[contains(@class, 'tm-quark-dropdown_type_6')]//*[contains(@class, 'tm-quark-dropdown__content_animate_show')]/ul/li[5]/button")
       WebElement signOutButton;

       public void clickOnMenu(){
              accountMenu.click();
       }
       public void clickToOutButton(){
              signOutButton.click();
       }
}
