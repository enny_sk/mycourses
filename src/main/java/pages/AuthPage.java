package pages;

import properties.User;
import services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by enny on 28.08.17.
 */
public class AuthPage {
    private final WebDriver driver;
    private CustomService service;

    public AuthPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = ".//input[@type='email']")
    private WebElement emailField;

    @FindBy(xpath = ".//*[@id='id-index-continue-button']//*[contains(@class, 'button_width_full button_height_large')]")
    private WebElement buttonContinueLogin;

    @FindBy(xpath = ".//input[@type='password']")
    private WebElement passwordField;

    @FindBy(xpath = ".//*[@id='id-password-login-button']//*[contains(@class, 'button_width_full')]")
    private WebElement loginButton;

    @FindBy(xpath = ".//*[@id='id-general-facebook-button']//*[contains(@class, 'button_bg_facebook')]")
    WebElement facebookLoginButton;

    public void enterEmail(String email){
        emailField.sendKeys(email);
    }

    public void clickContinue(){
        buttonContinueLogin.click();
    }

    public void waitForButtonLoginClicable(){
        service.waitClickable(loginButton);
    }

    public void enterPassword(String passwordTM){
        passwordField.sendKeys(passwordTM);
    }

    public void clickLogin() {
        loginButton.click();
    }
    public void clickOnFacebookLoginButton(){
        facebookLoginButton.click();
    }



    public void login(User user){
        enterEmail(user.getEmail());
        clickContinue();
        enterPassword(user.getPasswordTM());
        clickLogin();
    }
}
