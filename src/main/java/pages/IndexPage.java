package pages;

import services.CustomService;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by enny on 28.08.17.
 */
public class IndexPage {
    private final WebDriver driver;
    private CustomService service;

    public IndexPage(WebDriver driver) {
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy(xpath = ".//*[contains(@class, 'sub-menu container')]//*[contains(@class, 'sub-menu-1_dropdown')]/ul/li")
    private List<WebElement> subMenu;

    @FindBy(id = "categories-fashions")
    private WebElement categoriesFashions;

    @FindBy(xpath = ".//*[@id='tab-bestseller']//*[@class='container']//*[contains(@class, 'js-wizard-search-results')]/ul/li[3]//*[contains(@class, 'tm-icon')]")
    private WebElement heartOnThirdTemplate;

    @FindBy(id = "menu-favorites")
    public WebElement headerHeart;

    @FindBy(id = "header-signin-link")
    public WebElement signInLink;

    @FindBy (id = "categories")
    private List<WebElement> categories;

    @FindBy (id = "app-account-menu")
    WebElement accountMenu;

    public void waitForHeartEnable(){
        service.waitClickable(headerHeart);
    }
    public void clickOnSignLink(){

        signInLink.click();
    }
}